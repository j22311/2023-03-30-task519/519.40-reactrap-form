import { Button, Col, Container, Input, Label, Row } from "reactstrap";
import logo from '../../src/logo.svg';
function FormNhanVien() {
    return (
        <Container  >
            <Row>
            <h1 style={{textAlign:"center"}}>
                Hồ sơ Nhân Viên
            </h1>
            </Row>
           <Row>
                <Col className="border">
                    <Row className="mt-2">
                        <Col xs="3" className="border" style={{textAlign:"right"}}> 
                            <Label>Họ và tên</Label>
                        </Col>
                        <Col>
                            <Input ></Input>
                        </Col>
                    </Row>  
                    <Row className="mt-4">
                        <Col xs="3" className="border" style={{textAlign:"right"}}> 
                            <Label>Ngày sinh: </Label>
                        </Col>
                        <Col>
                            <Input ></Input>
                        </Col>
                    </Row>  
                    <Row className="mt-3">
                        <Col xs="3" className="border" style={{textAlign:"right"}}> 
                            <Label>Số điện thoại</Label>
                        </Col>
                        <Col>
                            <Input ></Input>
                        </Col>
                    </Row>  
                    <Row className="mt-3">
                        <Col xs="3" className="border" style={{textAlign:"right"}}> 
                            <Label>Giới tính:</Label>
                        </Col>
                        <Col >
                            <Input ></Input>
                        </Col>
                    </Row>  
                </Col>
                <Col xs="3" >
                    <img className="img-thumbnail" src={logo}></img>
                </Col>
           
           </Row>
           <Row className="mt-2">
                {/* <Col xs="3"  className="border" style={{textAlign:"right",width:"25%"}} >
                    <Label>Công việc: </Label>
                </Col> */}
                <div className="col-2" style={{textAlign:"right"}}>
                <label>Công việc</label>
                </div>
                <Col xs="10">
                    <textarea className="form-control" rows={5}></textarea>
                </Col>
           </Row>

           <Row>
                <Col className="mt-2 border " xs="7" style={{textAlign:"end"}}>
                    
                    <Button   color="primary" style={{width:"200px"}} >Chi Tiết</Button>
                </Col>
                <Col className="mt-2 border" xs="5" style={{textAlign:"center"}}>
                    <Button  color="primary" style={{width:"200px"}} >kiểm tra</Button>
                </Col>
                
           </Row>
        </Container>
    )
}
export default FormNhanVien;